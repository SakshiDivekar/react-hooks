import React, { useEffect, useLayoutEffect, useRef } from 'react'

function LayoutEffect() {
    const inputRef = useRef(null);

    useLayoutEffect(() =>{
        console.log(inputRef.current.value);

    }, []);

    useEffect(()=>{
        inputRef.current.value = "Hello";
    }, []);

    
  return (
    <>
      <div>
          <input ref={inputRef} value="Heyy" />
      </div>

    </>
  );
};

export default LayoutEffect;
