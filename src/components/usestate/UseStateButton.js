import React, { useState } from "react"; 

const State = () => {
    const [counter, setCounter] = useState(0);


    const decrement = () => {
        setCounter(counter-1);
    };

    const increment = () => {
        setCounter(counter+1);
    };
    return(
        <>
        <div>
        <button onClick={decrement}>Decrement</button>
            {counter}
            <button onClick={increment}>Increment</button>
        </div>
        </>
    );
}

export default State;
