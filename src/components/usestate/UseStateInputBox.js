import React, { useState } from "react";

const State = () => {

    const [inputValue, setInputVaule] = useState("Hello");

    const onChange = (event) => {
        const newValue = event.target.value;
        setInputVaule(newValue);
    };


    return(
        <>
            <div>
                <input placeholder="enter something...." onChange={onChange}/>
                {inputValue}
            </div>

        </>
    );
}

export default State;