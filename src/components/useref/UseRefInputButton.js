import React, { useRef } from "react";

function Ref() {
    const inputRef = useRef(null);
    const onClick = () => {
        inputRef.current.value = "";
       
    };

    return(
        <>
            <div>
                <input type="text" placeholder="Enter...." ref={inputRef} />
                <button onClick={onClick}>Change Number</button>
            </div>
        </>
    );
}

export default Ref;