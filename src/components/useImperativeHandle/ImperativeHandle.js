import React, {useRef} from 'react';
import Button from './Button';

function ImperativeHandle() {
    const buttonRef = useRef(null);

  return (
    <div>

        <buttom onClick={() => {
            buttonRef.current.alterToggle();
        }}>Buttom from Parent</buttom>
        <Button ref={buttonRef}/>
    </div>
  );
}

export default ImperativeHandle;
