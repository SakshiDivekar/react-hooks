import React, { forwardRef,useImperativeHandle, useState } from 'react'

const Button = forwardRef((ref, props) => {
const [toggle, setToggle] = useState(false);

useImperativeHandle(ref, () => ({
    alterToggle() {
    setToggle(!toggle);
    }
}));


  return (
    <>
    <button>Button from Child</button>
    {toggle && <span>Toggle</span>}
    </>
  );
});

export default Button;
